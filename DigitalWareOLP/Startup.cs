using DigitalWareOLP.DataAccess.ContextDB;
using DigitalWareOLP.DataAccess.Factory;
using DigitalWareOLP.DataAccess.Mappers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace DigitalWareOLP
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //swaggger configuration
            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "DigitalWareOLP service",
                    Version = "v1",
                    Description = "The HTTP Api DigitalWareOLP service",
                });
            });

            //-----------------------Register database and maps
            // Register Database Entity Maps:
            services.AddSingleton<IEntityTypeMap, ProductMapper>();
            services.AddSingleton<IEntityTypeMap, FactMapper>();
            services.AddSingleton<IEntityTypeMap, ClientMapper>();
            services.AddSingleton<IEntityTypeMap, ProductStockMapper>();
            services.AddSingleton<IEntityTypeMap, FactProductMapper>();

            var dbContextOptions = new DbContextOptionsBuilder<DigitalWareDBContext>()
                .UseSqlServer(Configuration["ConnectionStrings:DefaultConnection"])
                .Options;

            services.AddSingleton(dbContextOptions);
            // Finally register the DbContextOptions:
            services.AddSingleton<DigitalWareContextOptions>();
            // This Factory is used to create the DbContext from the custom DbContextOptions:
            services.AddSingleton<IContextDBFactory, ContextDBFactory>();
            // Finally Add the Applications DbContext:
            services.AddDbContext<DigitalWareDBContext>();
            //----------------------------------------------------

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger().UseSwaggerUI(e =>
            {
                e.SwaggerEndpoint("/swagger/v1/swagger.json", "DigitalWareOLP.Service");
            });
        }
    }
}
