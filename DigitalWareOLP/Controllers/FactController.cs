﻿using DigitalWareOLP.Business;
using DigitalWareOLP.Common.DTOs;
using DigitalWareOLP.DataAccess.Factory;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DigitalWareOLP.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class FactController: Controller
    {
        private readonly FactBS _factBS;
        public FactController(IContextDBFactory contextDBFactory) {
            _factBS = new FactBS(contextDBFactory);
        }

        [Route("get"), HttpGet]
        public async Task<IActionResult> Get(string guid)
        {
            try
            {
                var respSession = await _factBS.GetFact(guid);
                return Json(new
                {
                    response = true,
                    data = respSession,
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return ResponseJsonWithException(ex);
            }
        }

        [Route("register"), HttpPost]
        public async Task<IActionResult> Register([FromBody]FactDTO fact)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var respSession = await _factBS.RegisterFact(fact);
                return Json(new
                {
                    response = true,
                    data = respSession,
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return ResponseJsonWithException(ex);
            }
        }

        [Route("delete"), HttpDelete]
        public async Task<IActionResult> Delete([FromQuery]string guid)
        {
            try
            {
                var respSession = await _factBS.DeleteFact(guid);
                return Json(new
                {
                    response = true,
                    data = respSession,
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return ResponseJsonWithException(ex);
            }
        }

        private JsonResult ResponseJsonWithException(Exception ex)
        {
            //if (ex.InnerException != null)
            //_logger.LogError(ex.InnerException.ToString());

            return Json(new
            {
                response = false,
                data = false,
                error = ex.Message
            });
        }
    }
}
