﻿using DigitalWareOLP.Business;
using DigitalWareOLP.Common.DTOs;
using DigitalWareOLP.DataAccess.Factory;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DigitalWareOLP.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class ProductController: Controller
    {
        private readonly ProductBS _productBS;
        public ProductController(IContextDBFactory contextDBFactory) {
            _productBS = new ProductBS(contextDBFactory);
        }

        [Route("get"), HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var respSession = await _productBS.GetProduct(id);
                return Json(new
                {
                    response = true,
                    data = respSession,
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return ResponseJsonWithException(ex);
            }
        }

        [Route("register"),HttpPost]
        public async Task<IActionResult> Register([FromBody]ProductDTO product) {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var respSession = await _productBS.RegisterProduct(product);
                return Json(new
                {
                    response = true,
                    data = respSession,
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return ResponseJsonWithException(ex);
            }
        }

        [Route("update"), HttpPost]
        public async Task<IActionResult> Update([FromBody]ProductDTO product)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var respSession = await _productBS.UpdateProduct(product);
                return Json(new
                {
                    response = true,
                    data = respSession,
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return ResponseJsonWithException(ex);
            }
        }

        [Route("delete"), HttpDelete]
        public async Task<IActionResult> Delete([FromQuery]int id)
        {
            try
            {
                var respSession = await _productBS.DeleteProduct(id);
                return Json(new
                {
                    response = true,
                    data = respSession,
                    error = ""
                });
            }
            catch (Exception ex)
            {
                return ResponseJsonWithException(ex);
            }
        }

        private JsonResult ResponseJsonWithException(Exception ex)
        {
            //if (ex.InnerException != null)
                //_logger.LogError(ex.InnerException.ToString());

            return Json(new
            {
                response = false,
                data = false,
                error = ex.Message
            });
        }
    }
}
