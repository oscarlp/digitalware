﻿using DigitalWareOLP.DataAccess.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace DigitalWareOLP.DataAccess.ContextDB
{
    public class DigitalWareContextOptions {
        public readonly DbContextOptions<DigitalWareDBContext> Options;
        public readonly IEnumerable<IEntityTypeMap> Mappings;

        public DigitalWareContextOptions(DbContextOptions<DigitalWareDBContext> options, IEnumerable<IEntityTypeMap> mappings) {
            Options = options;
            Mappings = mappings;
        }
    }
    public class DigitalWareDBContext :DbContext
    {
        private readonly DigitalWareContextOptions options;
        public DigitalWareDBContext(DigitalWareContextOptions _options) : base(_options.Options)
        { this.options = _options; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            foreach (var mapping in options.Mappings)
            {
                mapping.Map(builder);
            }
            //options.DbContextSeed?.Seed(builder);
        }
    }

    public static class DigitalWareContextExtensions
    {
        public static DbSet<TEntityType> DbSet<TEntityType>(this DigitalWareDBContext context)
        where TEntityType : class
        {
            return context.Set<TEntityType>();
        }
    }
}
