﻿using DigitalWareOLP.Common.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DigitalWareOLP.DataAccess.Mappers
{
    public class ProductStockMapper : BaseEntityMapper<ProductStock>
    {
        protected override void InternalMap(EntityTypeBuilder<ProductStock> builder)
        {
            builder.ToTable("PRODUCT_STOCK");
            builder.HasKey(x => x.Id).HasName("ID");
            builder.Property(x => x.ProductId).HasColumnName("PRODUCT_ID");
            builder.Property(x => x.Stock).HasColumnName("STOCK").IsRequired();
        }
    }
}
