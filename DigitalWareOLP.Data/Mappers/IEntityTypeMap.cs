﻿using Microsoft.EntityFrameworkCore;

namespace DigitalWareOLP.DataAccess.Mappers
{
    public interface IEntityTypeMap
    {
        void Map(ModelBuilder builder);
    }
}
