﻿using DigitalWareOLP.Common.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DigitalWareOLP.DataAccess.Mappers
{
    public class ProductMapper : BaseEntityMapper<Product>
    {
        protected override void InternalMap(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("PRODUCT");
            builder.HasKey(x => x.Id).HasName("ID");
            builder.Property(x => x.Name).HasColumnName("NAME").IsRequired().HasMaxLength(60);
            builder.Property(x => x.BasePrice).HasColumnName("BASE_PRICE").IsRequired();
            builder.Property(x => x.OriginCompany).HasColumnName("ORIGIN_COMPANY").IsRequired().HasMaxLength(100);
            builder.Property(x => x.ProductType).HasColumnName("PRODUCT_TYPE").IsRequired().HasMaxLength(60);
            builder.Property(x => x.DateProduced).HasColumnName("DATE_PRODUCED").IsRequired();
            builder.Property(x => x.ExpirationDate).HasColumnName("EXPIRATION_DATE");
        }
    }
}
