﻿using DigitalWareOLP.Common.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DigitalWareOLP.DataAccess.Mappers
{
    public class FactMapper : BaseEntityMapper<Fact>
    {
        protected override void InternalMap(EntityTypeBuilder<Fact> builder)
        {
            builder.ToTable("FACT");
            builder.HasKey(x => x.Id).HasName("ID");
            builder.Property(x => x.IdClient).HasColumnName("ID_CLIENT");
            builder.Property(x => x.GenerateDate).HasColumnName("GENERATE_DATE").IsRequired();
            builder.Property(x => x.PaymentType).HasColumnName("PAYMENT_TYPE").IsRequired();
            builder.Property(x => x.OriginStore).HasColumnName("ORIGIN_STORE").HasMaxLength(100);
        }
    }
}
