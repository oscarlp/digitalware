﻿using DigitalWareOLP.Common.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DigitalWareOLP.DataAccess.Mappers
{
    public class FactProductMapper : BaseEntityMapper<FactProduct>
    {
        protected override void InternalMap(EntityTypeBuilder<FactProduct> builder)
        {
            builder.ToTable("FACT_PRODUCT");
            builder.HasKey(x => new { x.IdFact, x.IdProduct });
            builder.Property(x => x.IdFact).HasColumnName("ID_FACT");
            builder.Property(x => x.IdProduct).HasColumnName("ID_PRODUCT");
            builder.Property(x => x.PurchasePrice).HasColumnName("PURCHASE_PRICE").IsRequired();
            builder.Property(x => x.PercentDiscount).HasColumnName("PERCENT_DISCOUNT");
            builder.Property(x => x.Quantity).HasColumnName("QUANTITY").IsRequired();
            
        }
    }
}
