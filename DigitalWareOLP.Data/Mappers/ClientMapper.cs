﻿using DigitalWareOLP.Common.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DigitalWareOLP.DataAccess.Mappers
{
    public class ClientMapper : BaseEntityMapper<Client>
    {
        protected override void InternalMap(EntityTypeBuilder<Client> builder)
        {
            builder.ToTable("CLIENT");
            builder.HasKey(x => x.Id).HasName("ID");
            builder.Property(x => x.DocumentNumber).HasColumnName("DOCUMENT_NUMBER").IsRequired().HasMaxLength(60);
            builder.Property(x => x.FirstName).HasColumnName("FIRST_NAME").HasMaxLength(60);
            builder.Property(x => x.LastName).HasColumnName("LAST_NAME").HasMaxLength(60);
            builder.Property(x => x.BornDate).HasColumnName("BORN_DATE");
            builder.Property(x => x.Email).HasColumnName("EMAIL").HasMaxLength(100);
            builder.Property(x => x.ContactNumber).HasColumnName("CONTACT_NUMBER").HasMaxLength(30);
        }
    }
}
