﻿using DigitalWareOLP.DataAccess.ContextDB;

namespace DigitalWareOLP.DataAccess.Factory
{
    public class ContextDBFactory : IContextDBFactory
    {
        private readonly DigitalWareContextOptions options;

        public ContextDBFactory(DigitalWareContextOptions options)
        {
            this.options = options;
        }
        public DigitalWareDBContext Process()
        {
            return new DigitalWareDBContext(options);
        }
    }
}
