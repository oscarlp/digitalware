﻿using DigitalWareOLP.DataAccess.ContextDB;

namespace DigitalWareOLP.DataAccess.Factory
{
    public interface IContextDBFactory
    {
        DigitalWareDBContext Process();
    }
}
