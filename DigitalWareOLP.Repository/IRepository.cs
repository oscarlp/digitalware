﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace DigitalWareOLP.Repository
{
    public interface IRepository<TEntity>
       where TEntity : class
    {
        Task<TEntity> Get(object id);
        Task<List<TEntity>> List();
        Task<bool> Create(TEntity entity);
        Task<TEntity> Update(TEntity entity);
        Task<bool> Delete(TEntity entity);
        Task<int> ExecuteSP(DbCommand cmd);
    }
}
