﻿using DigitalWareOLP.DataAccess.ContextDB;
using DigitalWareOLP.DataAccess.Factory;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace DigitalWareOLP.Repository
{
    /*Se heredará de tal forma debido a que también se pueden crear otros tipos de repositorios que envien de forma implicita el modelo
    y no una clase generica TEntity*/

    /// <summary>
    /// Repositorio Base
    /// </summary>
    /// <typeparam name="TEntity">Entidad</typeparam>
    /// <typeparam name="TContext">Contexto</typeparam>
    public class Repository<TEntity> : IRepository<TEntity>
      where TEntity : class
    {
        private readonly IContextDBFactory dbContextFactory;

        /// <summary>
        /// Inicializa una nueva instancia de un servicio de manipulación de datos sobre una entidad de negocio en particular.
        /// </summary>
        /// <param name="repository">Repositorio de datos que controla las operaciones de manipulación de datos de la entidad.</param>
        public Repository(IContextDBFactory dbContextFactory)
        {
            this.dbContextFactory = dbContextFactory;
        }

        /// <summary>
        /// Devuelve el objeto que corresponde a un identificador o valor clave específico.
        /// </summary>
        /// <param name="id">Valor clave identificador del objeto.</param>
        /// <returns>Objeto que corresponde al identificador especificado.</returns>
        public async Task<TEntity> Get(object id)
        {
            if (dbContextFactory != null)
            {
                try
                {
                    using (var context = dbContextFactory.Process())
                    {
                        return await context.DbSet<TEntity>().FindAsync(id);
                    }
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.Message);
                }
            }
            else
            {
                throw new ArgumentException("Error. Context DB cannot be null.");
            }
            return null;
        }

        /// <summary>
        /// Devuelve una colección con todos los elementos de la entidad que se encuentran almacenados en el origen de datos. 
        /// </summary>
        /// <returns>Conjunto de elementos de la entidad existentes en el origen de datos.</returns>
        public async Task<List<TEntity>> List()
        {
            try
            {
                if (dbContextFactory != null)
                {
                    using (var context = dbContextFactory.Process())
                    {
                        return await context.DbSet<TEntity>().ToListAsync(); ;
                    }
                }
                else
                {
                    throw new ArgumentException("Error. Context DB cannot be null.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Registra un elemento del tipo entidad en el origen de datos. 
        /// </summary>
        /// <returns>Resultado bool del registro en el origen de datos.</returns>
        public async Task<bool> Create(TEntity entity)
        {
            bool result = false;
            try
            {
                if (dbContextFactory != null && entity != null)
                {
                    using (var context = this.dbContextFactory.Process())
                    {
                        await context.DbSet<TEntity>().AddAsync(entity);
                        await context.SaveChangesAsync();
                    }
                    result = true;
                }
                else
                    throw new ArgumentException("Error. Context DB or entity cannot be null.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// Actualiza un elemento del tipo entidad en el origen de datos. 
        /// </summary>
        /// <returns>Entidad actualizada en el origen de datos.</returns>
        public async Task<TEntity> Update(TEntity entity)
        {
            try
            {
                if (dbContextFactory != null && entity != null)
                {
                    //this.EntitySet.Attach(entity);                  
                    using (var context = this.dbContextFactory.Process())
                    {
                        context.DbSet<TEntity>().Update(entity);
                        await context.SaveChangesAsync();
                    }
                    return entity;
                }
                else
                {
                    throw new ArgumentException("Error. Context DB or entity cannot be null.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        /// <summary>
        /// Elimina un elemento del tipo entidad del origen de datos. 
        /// </summary>
        /// <returns>Resultado bool del borrado del registro en el origen de datos.</returns>
        public async Task<bool> Delete(TEntity entity)
        {
            try
            {
                if (dbContextFactory != null && entity != null)
                {
                    using (var context = this.dbContextFactory.Process())
                    {
                        context.Remove(entity);
                        await context.SaveChangesAsync();
                        return true;
                    }
                }
                else
                {
                    throw new ArgumentException("Error. Context DB or entity cannot be null.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;
        }

        public async Task<int> ExecuteSP(DbCommand cmd)
        {
            int result = -1;
            try
            {
                if (cmd.Connection.State != ConnectionState.Open)
                {
                    cmd.Connection.Open();
                    var resp = await cmd.ExecuteScalarAsync();
                    result = (resp != null) ? Convert.ToInt32(resp) : -1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return result;
        }
    }
}
