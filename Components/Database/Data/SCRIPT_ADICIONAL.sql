-------------------------------------------------
--AUTOR:	OSCAR LOZANO
--FECHA:	
--FUNCION:	OBTENER LISTA PRECIOS TODOS LOS PRODUCTOS
--CIA:		DIGITALWARE-SQLSERVER
-------------------------------------------------

SELECT *FROM dbo.PRODUCT ORDER BY [NAME]

-------------------------------------------------
--AUTOR:	OSCAR LOZANO
--FECHA:	
--FUNCION:	OBTENER LISTA PRODUCTS CON STOCK MAYOR O IGUAL A 5
--CIA:		DIGITALWARE-SQLSERVER
-------------------------------------------------
SELECT *FROM dbo.PRODUCT pro
INNER JOIN dbo.PRODUCT_STOCK st ON pro.ID = st.PRODUCT_ID
WHERE st.STOCK >= 5

-------------------------------------------------
--AUTOR:	OSCAR LOZANO
--FECHA:	
--FUNCION:	OBTENER LISTA CLIENTES MAYORES 35 ANIOS Y QUE HAYAN COMPRADO ENTRE 01/02/2000 Y 25/05/2000 
--CIA:		DIGITALWARE-SQLSERVER
-------------------------------------------------
SELECT cl.* FROM dbo.CLIENT cl
INNER JOIN dbo.FACT fc ON fc.ID_CLIENT= cl.ID_GUID
WHERE (cast(convert(varchar(8),getdate(),112) as int)-
		cast(convert(varchar(8),cl.BORN_DATE,112) as int)) >=35 
	AND (SELECT COUNT(*) FROM dbo.FACT WHERE ID_CLIENT = cl.ID_GUID AND convert(DATE,GENERATE_DATE) BETWEEN convert(DATE,'20000201') AND convert(DATE,'20000525')) > 0 


	-------------------------------------------------
--AUTOR:	OSCAR LOZANO
--FECHA:	
--FUNCION:	MUESTRA TOTAL VENDIDO POR PRODUCTO EN ANIO 2000
--CIA:		DIGITALWARE-SQLSERVER
-------------------------------------------------
SELECT pro.NAME, SUM((fp.PURCHASE_PRICE * fp.QUANTITY) * (1-fp.PERCENT_DISCOUNT)) AS 'TOTAL_SELLED' FROM dbo.PRODUCT pro
INNER JOIN dbo.FACT_PRODUCT fp ON fp.ID_PRODUCT = pro.ID
INNER JOIN dbo.FACT fc ON fc.ID_GUID= fp.GUID_FACT
WHERE DATEPART(year, fc.GENERATE_DATE) = 2000  GROUP BY pro.NAME