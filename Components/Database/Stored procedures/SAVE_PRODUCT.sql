-------------------------------------------------
--AUTOR:	OSCAR LOZANO
--FECHA:	
--FUNCION:	REGISTRA PRODUCT Y REGISTRA STOCK DEL MISMO
--CIA:		DIGITALWARE-SQLSERVER
-------------------------------------------------
IF EXISTS(SELECT  *FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SAVE_PRODUCT')AND type IN ( N'P', N'PC' ))
BEGIN
	 DROP PROCEDURE SAVE_PRODUCT
	 PRINT 'DROPED PROCEDURE SAVE_PRODUCT...'
END
GO

CREATE PROCEDURE SAVE_PRODUCT(
	@NAME varchar(60),
	@BASE_PRICE DECIMAL(18,2),
	@ORIGIN_COMPANY VARCHAR(100),
	@PRODUCT_TYPE VARCHAR(60),
	@DATE_PRODUCED DATETIME,
	@EXPIRATION_DATE DATETIME =null,
	@STOCK INT 
)
AS
	DECLARE @ID INT

	INSERT INTO dbo.PRODUCT VALUES(@NAME, @BASE_PRICE, @ORIGIN_COMPANY, @PRODUCT_TYPE, @DATE_PRODUCED, @EXPIRATION_DATE)
	SELECT @ID= ID FROM dbo.PRODUCT WHERE [NAME] = @NAME

	INSERT INTO dbo.PRODUCT_STOCK VALUES(@ID, @STOCK)
	
	IF(@@ERROR <> 0) AND ISNULL(@ID,0) = 0
		SELECT 0
	ELSE
		SELECT 1
GO

IF EXISTS(SELECT  *FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SAVE_PRODUCT')AND type IN ( N'P', N'PC' ))
	PRINT 'CREATED PROCEDURE SAVE_PRODUCT...'
GO