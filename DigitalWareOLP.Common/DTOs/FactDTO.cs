﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace DigitalWareOLP.Common.DTOs
{
    [DataContract]
    public class FactDTO
    {
        [Required]
        public string DocumentClient { get; set; }

        [Required]
        public string OriginStore { get; set; }

        [Required]
        public string PaymentType { get; set; }

        [Required]
        public List<ProductPay> Products { get; set; }
    }
}
