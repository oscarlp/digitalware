﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace DigitalWareOLP.Common.DTOs
{
    [DataContract]
    public class ProductPay
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public decimal PercentDiscount { get; set; }
    }
}
