﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace DigitalWareOLP.Common.DTOs
{
    [DataContract]
    public class ProductDTO
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public decimal BasePrice { get; set; }

        [Required]
        public string OriginCompany { get; set; }

        [Required]
        public string ProductType { get; set; }

        [Required]
        public DateTime DateProduced { get; set; }

        [Required]
        [Range(5, 1000000)]
        public int Stock { get; set; }

        public DateTime ExpirationDate { get; set; }

    }
}
