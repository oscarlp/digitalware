﻿
namespace DigitalWareOLP.Common.Models
{
    public class FactProduct
    {
        public string IdFact { get; set; }
        public int IdProduct { get; set; }
        public decimal PurchasePrice { get; set; }
        public decimal PercentDiscount { get; set; }
        public int Quantity { get; set; }
    }
}
