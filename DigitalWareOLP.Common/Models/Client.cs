﻿
using System;

namespace DigitalWareOLP.Common.Models
{
    public class Client
    {
        public string Id {get;set;}
        public string DocumentNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BornDate { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
    }
}
