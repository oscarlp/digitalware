﻿using System;

namespace DigitalWareOLP.Common.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal BasePrice { get; set; }
        public string OriginCompany { get; set; }
        public string ProductType { get; set; }
        public DateTime DateProduced { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}
