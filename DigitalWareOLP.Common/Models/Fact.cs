﻿
using System;
using System.Runtime.Serialization;

namespace DigitalWareOLP.Common.Models
{
    public class Fact
    {
        public string Id { get; set; }
        public string IdClient { get; set; }
        public DateTime GenerateDate { get; set; }
        public string OriginStore { get; set; }
        public string PaymentType { get; set; }

    }
}
