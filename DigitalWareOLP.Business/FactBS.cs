﻿using DigitalWareOLP.Common.DTOs;
using DigitalWareOLP.Common.Models;
using DigitalWareOLP.DataAccess.ContextDB;
using DigitalWareOLP.DataAccess.Factory;
using DigitalWareOLP.Repository;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalWareOLP.Business
{
    public class FactBS : Repository<Fact>
    {
        private readonly IContextDBFactory _contextDBfactory;
        private readonly FactProductBS _factProductBS;
        private readonly ClientBS _clientBS;
        public FactBS(IContextDBFactory contextDBFactory) : base(contextDBFactory) {
            _contextDBfactory = contextDBFactory;
            _factProductBS = new FactProductBS(contextDBFactory);
            _clientBS = new ClientBS(contextDBFactory);
        }


        public async Task<Fact> GetFact(string guid)
        {
            try
            {
                var fact =  await Task.Run(() =>
                {
                    using (var context = this._contextDBfactory.Process())
                    {
                        var result = (from fact in context.DbSet<Fact>()
                                      where fact.Id == guid
                                      select fact
                                    ).ToList();
                        return result.FirstOrDefault();
                    }
                });
                if (fact == null)
                    throw new ArgumentException(Errors.FactNotExists);

                return fact;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    throw new ArgumentException(Errors.ErrorRegisterFact, ex);
                else
                    throw ex;
            }
        }

        public async Task<bool> RegisterFact(FactDTO entity)
        {
            try
            {
                Fact fact = new Fact();
                fact.PaymentType = entity.PaymentType;
                fact.Id = Guid.NewGuid().ToString();
                fact.GenerateDate = DateTime.Now;
                var clResult = await _clientBS.GetClientByDocument(entity.DocumentClient);
                fact.IdClient = clResult.Id;

                fact.OriginStore = entity.OriginStore;
                await this.Create(fact);

                foreach (var product in entity.Products) {
                   await _factProductBS.RegisterFactProduct(product, fact.Id);
                }

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    throw new ArgumentException(Errors.ErrorRegisterFact, ex);
                else
                    throw ex;
            }
        }

        public async Task<bool> DeleteFact(string guid)
        {
            try
            {
                var fact = await GetFact(guid);

                if (fact == null)
                    throw new ArgumentException(Errors.FactNotExists);

                return await this.Delete(fact);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    throw new ArgumentException(Errors.ErrorDeleteFact, ex);
                else
                    throw ex;
            }
        }
    }
}
