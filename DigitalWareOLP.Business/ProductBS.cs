﻿using DigitalWareOLP.Business.Mappers;
using DigitalWareOLP.Common.DTOs;
using DigitalWareOLP.Common.Models;
using DigitalWareOLP.DataAccess.Factory;
using DigitalWareOLP.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace DigitalWareOLP.Business
{
    public class ProductBS :Repository<Product>
    {
        private readonly IContextDBFactory _dbContextFactory;
        public ProductBS(IContextDBFactory contextDBFactory) : base (contextDBFactory) {
            this._dbContextFactory = contextDBFactory;
        }

        public async Task<Product> GetProduct(int id)
        {
            try
            {
                var product = await this.Get(id);
                if (product == null)
                    throw new ArgumentException(Errors.ProductNotExists);

                return product;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    throw new ArgumentException(Errors.ErrorGetProduct, ex);
                else
                    throw ex;
            }
        }

        public async Task<bool> RegisterProduct(ProductDTO entity) {
            try
            {
                Product product = new Product();
                product.ProductType = entity.ProductType;
                product.Name = entity.Name;
                product.OriginCompany = entity.OriginCompany;
                product.DateProduced = entity.DateProduced;
                product.ExpirationDate = entity.ExpirationDate;
                product.BasePrice = entity.BasePrice;

                var result = -1; //the sp return id of entity
                DbCommand cmd = this._dbContextFactory.Process().Database.GetDbConnection().CreateCommand();
                var cmd2 = ProductBSMapper.SPRegisterProduct(cmd, product, entity.Stock);
                result = await ExecuteSP(cmd2);

                if (result < 0)
                    throw new ArgumentException(Errors.ErrorRegisterProduct);

                return true;
            }catch (Exception ex)
            {
                if (ex.InnerException != null)
                    throw new ArgumentException(Errors.ErrorRegisterProduct, ex);
                else
                    throw ex;
            }
        }

        public async Task<bool> DeleteProduct(int id)
        {
            try
            {
                var product = await this.Get(id);
                if (product == null)
                    throw new ArgumentException(Errors.ProductNotExists);

                return await this.Delete(product);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    throw new ArgumentException(Errors.ErrorDeleteProduct, ex);
                else
                    throw ex;
            }
        }

        public async Task<bool> UpdateProduct(ProductDTO product)
        {
            try
            {
                Product current = await this.Get(product.Id);
                if (current == null)
                    throw new ArgumentException(Errors.ProductNotExists);

                current.BasePrice= product.BasePrice;
                current.DateProduced = product.DateProduced;
                current.ExpirationDate = product.ExpirationDate;
                current.Name = product.Name;
                current.OriginCompany = product.OriginCompany;
                current.ProductType = product.ProductType;

                await this.Update(current);

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    throw new ArgumentException(Errors.ErrorUpdateProduct, ex);
                else
                    throw ex;
            }
        }
    }
}
