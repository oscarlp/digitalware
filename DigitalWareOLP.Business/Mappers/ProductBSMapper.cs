﻿using DigitalWareOLP.Common.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace DigitalWareOLP.Business.Mappers
{
    public static class ProductBSMapper
    {
        public static DbCommand SPRegisterProduct(DbCommand cmd, Product entity, int stock) {
            if (entity == null)
                return null;

            cmd.CommandText = "SAVE_PRODUCT";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("NAME", SqlDbType.VarChar) { Value = entity.Name });
            cmd.Parameters.Add(new SqlParameter("BASE_PRICE", SqlDbType.Decimal) { Value = entity.BasePrice});
            cmd.Parameters.Add(new SqlParameter("ORIGIN_COMPANY", SqlDbType.VarChar) { Value = entity.OriginCompany });
            cmd.Parameters.Add(new SqlParameter("PRODUCT_TYPE", SqlDbType.VarChar) { Value = entity.ProductType });
            cmd.Parameters.Add(new SqlParameter("DATE_PRODUCED", SqlDbType.DateTime) { Value = entity.DateProduced });
            cmd.Parameters.Add(new SqlParameter("EXPIRATION_DATE", SqlDbType.DateTime) { Value = entity.ExpirationDate });
            cmd.Parameters.Add(new SqlParameter("STOCK", SqlDbType.Int) { Value = stock });

            return cmd;

        }
    }
}
