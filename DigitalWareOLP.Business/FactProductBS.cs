﻿using DigitalWareOLP.Common.DTOs;
using DigitalWareOLP.Common.Models;
using DigitalWareOLP.DataAccess.ContextDB;
using DigitalWareOLP.DataAccess.Factory;
using DigitalWareOLP.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalWareOLP.Business
{
    public class FactProductBS: Repository<FactProduct>
    {
        private readonly IContextDBFactory _contextDBfactory;
        public FactProductBS(IContextDBFactory contextDBFactory) : base(contextDBFactory)
        {
            _contextDBfactory = contextDBFactory;
        }

        public async Task<bool> RegisterFactProduct(ProductPay productPay, string factId) {
            try
            {
                FactProduct entity = new FactProduct();
                entity.IdFact = factId;
                entity.IdProduct = productPay.Id;
                entity.PercentDiscount = productPay.PercentDiscount;
                //validamos no superar stock
                entity.Quantity = productPay.Quantity;
                int stock = await Task.Run(() =>
                {
                    using (var context = this._contextDBfactory.Process())
                    {
                        var result = (from prod in context.DbSet<ProductStock>()
                                      where prod.ProductId == productPay.Id
                                      select prod.Stock
                                    ).ToList();
                        return result.FirstOrDefault();
                    }
                });
                if(stock< productPay.Quantity)
                    throw new ArgumentException(Errors.StockInsufficient);

                //buscamos precio actual
                entity.PurchasePrice= await Task.Run(() =>
                {
                    using (var context = this._contextDBfactory.Process())
                    {
                        var result = (from prod in context.DbSet<Product>()
                                      where prod.Id == entity.IdProduct
                                      select prod.BasePrice
                                    ).ToList();
                        return result.FirstOrDefault();
                    }
                });

                await this.Create(entity);

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    throw new ArgumentException(Errors.ErrorRegisterFact, ex);
                else
                    throw ex;
            }
        }
    }
}
