﻿using DigitalWareOLP.Common.Models;
using DigitalWareOLP.DataAccess.ContextDB;
using DigitalWareOLP.DataAccess.Factory;
using DigitalWareOLP.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalWareOLP.Business
{
    public class ClientBS : Repository<Client>
    {
        private readonly IContextDBFactory _contextDBfactory;
        public ClientBS(IContextDBFactory contextDBFactory) : base(contextDBFactory)
        {
            _contextDBfactory = contextDBFactory;
        }

        public async Task<Client> GetClientByDocument(string document) {
            try
            {
                var client = await Task.Run(() =>
                {
                    using (var context = this._contextDBfactory.Process())
                    {
                        var result = (from client in context.DbSet<Client>()
                                      where client.DocumentNumber == document
                                      select client
                                    ).ToList();
                        return result.FirstOrDefault();
                    }
                });
                if (client == null)
                    throw new ArgumentException(Errors.ClientNotExists);

                return client;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    throw new ArgumentException(Errors.ErrorRegisterFact, ex);
                else
                    throw ex;
            }
        }
    }
}
